# Wow 3.3.5 WeakAuras Strings Collection

A collection of import strings for WeakAuras 3.3.5.

## How to import a weakaura
Checkout the [wiki](https://gitlab.com/official.noobsmoke/wow-3.3.5-weakauras-strings-collection/-/wikis/How-to-import-a-weakaura) on how to import a weakaura.

## Social Media
- [Discord](https://discord.gg/tpyUYVkG7B)
- [Youtube](https://www.youtube.com/channel/UCKgtlZXXsWco5HFgubMFbjg)
- [Twitch](https://www.twitch.tv/noobsmoke)
- [Facebook](https://www.facebook.com/OfficialNoobsmoke)
- [Gmail](mailto:official.noobsmoke@gmail.com)

